# messy-example-project

Messy example project for the module 5 RDM workshop

- **wheelchair_sprints0** contains the messy data for exercise 1
- **wheelchair_sprints1** is *a* solution to exercise 1 for exercise 2
- **wheelchair_sprints2** is *a* solution to exercise 2 for exercise 3
- **wheelchair_sprints3** is the final solution, note that the container should be outside of the project files
- container is a provided VeraCrypt container
