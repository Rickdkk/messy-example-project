# Wheelchair sprints

In this projects a strength training intervention is used to improve the 100m sprint performance of novice wheelchair racers. The intervention consists of three weeks of bench press taining based on Smolov jr. It is long known that having a big bench will improve all aspects of life, so it is likely this will also be the case for wheelchair racing. Therefore, it is expected that participants will improve their sprint performance (reduce sprint time) between the pre- and post-test. Data were collected by students using a stopwatch on a local sports track. All documents, data, and scripts are included in the project directory.

## Project organization

The project structure distinguishes three kinds of folders:

- read-only (RO): not edited by either code or researcher
- human-writeable (HW): edited by the researcher only.
- project-generated (PG): folders generated when running the code; these folders can be deleted or emptied and will be completely reconstituted as the project is run.

TODO: define project structure

```
.
├── 
├── 
│   └── 
├── 
│   ├── 
│   └── 
└── 

```

## Prerequisites

Plots are generated using Python 3.6+. The required packages can be installed with (preferably in a new environment):

`pip install -r requirements.txt`

## License

This project is licensed under the terms of the [MIT](/LICENSE.md)

## Citation

Please [cite this project as described here](/CITATION.md).
