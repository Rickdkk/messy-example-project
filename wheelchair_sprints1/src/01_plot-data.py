"""Script to make plots for the manuscript"""

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

plt.style.use(["science", "scatter"])

# %% read data
long = pd.read_csv("../data/raw/data_long.csv")

# %% plot 1
g = sns.swarmplot(x="time", y="sprint_time", hue="sex", data=long)
g.set_xlabel("")
g.set_ylabel("Sprint time [s]")
g.set_title("Sprint performance over time")

legend = g.legend(loc="upper left")  # legend is already present but I want it outside
legend.set_bbox_to_anchor([0.99, 1.05])
legend.set_title("Sex")

plt.savefig("../results/figures/sprint_vs_time.png", facecolor="white", dpi=600)

# %% plot 2
g = sns.scatterplot(x="weight_kg", y="sprint_time", hue="sex", data=long)
g.set_xlabel("Weight [kg]")
g.set_ylabel("Sprint time [s]")
g.set_title("Sprint performance versus bodyweight")

legend = g.legend(loc="upper left")
legend.set_bbox_to_anchor([0.99, 1.05])
legend.set_title("Sex")

plt.savefig("../results/figures/sprint_vs_weight.png", facecolor="white", dpi=600)

