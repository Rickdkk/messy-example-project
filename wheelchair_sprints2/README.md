# Wheelchair sprints

In this projects a strength training intervention is used to improve the 100m sprint performance of novice wheelchair racers. The intervention consists of three weeks of bench press taining based on Smolov jr. It is long known that having a big bench will improve all aspects of life, so it is likely this will also be the case for wheelchair racing. Therefore, it is expected that participants will improve their sprint performance (reduce sprint time) between the pre- and post-test. Data were collected by students using a stopwatch on a local sports track. All documents, data, and scripts are included in the project directory.

## Project organization

The project structure distinguishes three kinds of folders:

- read-only (RO): not edited by either code or researcher
- human-writeable (HW): edited by the researcher only.
- project-generated (PG): folders generated when running the code; these folders can be deleted or emptied and will be completely reconstituted as the project is run.

```
.
├── .gitignore         <- Untracked files that git will ignore
├── CITATION.cff       <- Contains information on how to cite the project
├── LICENSE            <- Copyright information
├── README.md
├── requirements.txt   <- Requirements for reproducing the analysis environment (PG)
├── data               <- All project data, ignored by git
│   ├── processed      <- The final, canonical data sets for modeling. (PG)
│   ├── raw            <- The original, immutable data dump. (RO)
│   └── temp           <- Intermediate data that has been transformed. (PG)
├── docs               <- Documentation notebook for users (HW)
│   ├── manuscript     <- Manuscript source, e.g., LaTeX, Markdown, etc. (HW)
│   └── reports        <- Other project reports and notebooks (e.g. Jupyter, .Rmd) (HW)
├── results
│   ├── figures        <- Figures for the manuscript or reports (PG)
│   └── output         <- Other output for the manuscript or reports (PG)
└── src                <- Source code for this project (HW)

```

## Prerequisites

Plots are generated using Python 3.6+. The required packages can be installed with (preferably in a new environment):

`pip install -r requirements.txt`

## License

This project is licensed under the terms of the [MIT](/LICENSE).

## Citation

Please [cite this project as described here](/CITATION.cff).
